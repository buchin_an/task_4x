package com.company;

public class Main {

    public static void main(String[] args) {
        String string = "Шалаш";

        if (string.compareToIgnoreCase(new StringBuilder(string).reverse().toString()) == 0) {
            System.out.println("палиндром");
        } else System.out.println("не палиндром");

        string = "Аргентина манит негра!";

        String str = string
                .replace(" ", "")
                .replace(".", "")
                .replace("!", "")
                .replace("?", "");


        if (str.compareToIgnoreCase(new StringBuilder(str).reverse().toString()) == 0) {
            System.out.println("палиндром");
        } else System.out.println("не палиндром");
    }

}
